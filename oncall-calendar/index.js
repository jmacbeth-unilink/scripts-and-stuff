const request = require('request');
require('dotenv').config();

const forwarderUrl = "http://intra.bconline.co.uk/tel-fwd.php";
const confluenceUrl = "https://unilinksw.atlassian.net/wiki";
const endpoint = "/rest/calendar-services/1.0/calendar/events.json";

let calendarId = "062266f0-c849-44b8-9c59-3c4515918541";
let today = new Date();
today.setHours(0, 0, 0, 0);

let tomorrow = new Date();
tomorrow.setDate(today.getDate() + 1);
tomorrow.setHours(0, 0, 0, 0);

if (!process.env.CONF_USERNAME || !process.env.CONF_PASSWORD) {
    console.error("Username and/or Password env vars not set!");
    process.exit(1);
}

let requestUri = `${confluenceUrl}${endpoint}?subCalendarId=${calendarId}&userTimeZoneId=Europe%2FLondon` + 
                `&start=${today.toISOString().split('.')[0] + "Z"}&end=${tomorrow.toISOString().split('.')[0] + "Z"}`;


request(requestUri, { json: true, auth: {user: process.env.CONF_USERNAME, pass: process.env.CONF_PASSWORD} }, (err, res, body) => {
    let oncallUser = body.events[0].invitees[0].displayName;

    console.log("Current on-call staff member is:");
    console.log(oncallUser);

    let requestUsername;

    switch(oncallUser) {
        case "Michael Wetherall":
            requestUsername = "michaelw";
            break;
        case "Joseph Dundon":
            requestUsername = "joed";
            break;
        case "Robert McCormack":
            requestUsername = "robm";
            break;
        case "James MacBeth":
            requestUsername = "jamesm";
            break;
        default:
            requestUsername = "michaelw";
    }

    request.post(forwarderUrl, { form:{contact: requestUsername} }, (postErr, postRes, postBody) => {
        if (postErr) console.error(postErr);
        if (postRes.statusCode == 200) console.log(`Forwarder successfully set to ${requestUsername}\n`);
    });
});
