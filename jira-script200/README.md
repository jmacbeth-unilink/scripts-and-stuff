# jira-script200

Given Jira credentials and a Jira ticket number, parses a standard Script 200 ticket for CRNs and Probation Areas and generates a sequence of SQL statements to run to insert into the MISSING_ALLOCATED_OFFENDERS table.

Just to speed things up/make life a little easier with large Script 200 tickets.
