const request = require('request');
require('dotenv').config();

const TEST_MODE = process.env.TEST_MODE ? process.env.TEST_MODE : false;
const OPERATION = process.env.OPERATION ? process.env.OPERATION : 'release';

const jiraUrl = 'https://jira.engineering-dev.probation.hmpps.dsd.io/rest/api/2';
const project = 'DST';


request(`${jiraUrl}/project/${project}/versions`, {json: true, auth: {user: process.env.CONF_USERNAME, pass: process.env.CONF_PASSWORD}}, 
    (err, res, body) => {
        if (res.statusCode != 200) {
            console.log("no versions found.");
            return;
        }

        if (OPERATION === 'archive')
        {
            prepareArchive(body)
        } else {
            body.forEach(el => {
                if (el.name.includes('ND') || el.name.includes('UMT')) {
                    console.log(`${el.name} - ${el.released}`);

                    if (!el.released) {
                        if (TEST_MODE) {
                            console.log(`[TEST_MODE] Would mark ${el.name} as released.`);
                        } else {
                            markVersionReleased(el.id);
                        }
                    }
                }
            });
        }
});

function prepareArchive(body) {
    body.forEach(el => {

        // Archive ND versions that are not ND 6.x.x
        if (el.name.includes('ND') && !el.name.includes('ND 6.')) {

            // console.log(`${el.name} - ${el.archived}`);

            if (!el.archived) {
                if (TEST_MODE) {
                    console.log(`[TEST_MODE] Would mark ${el.name} as archived.`);
                } else {
                    archiveVersion(el.id);
                }
            }
        }
    });
}

function markVersionReleased(id) {
    if (!id) {
        console.log("invalid version id provided");
        return;
    }

    request({method: 'PUT', uri: `${jiraUrl}/version/${id}`, json: {'released': true}, auth: {user: process.env.CONF_USERNAME, pass: process.env.CONF_PASSWORD}}, (err, res, body) => {
        if (body)
            console.log(body);
    });
}

function archiveVersion(id) {
    if (!id) {
        console.log("invalid version id provided");
        return;
    }

    request({method: 'PUT', uri: `${jiraUrl}/version/${id}`, json: {'archived': true, 'released': true}, auth: {user: process.env.CONF_USERNAME, pass: process.env.CONF_PASSWORD}}, (err, res, body) => {
        if (body)
            console.log(body);
    });
}