// Input is expected in the format:
// 7FE8CAE41E60 00010000 00000000 41445055 4E204554  [........UPDATE N]

val input = """
7FE8CAE41E60 00010000 00000000 41445055 4E204554  [........UPDATE N]
7FE8CAE41E70 53204953 4C205445 5F545341 41445055  [SI SET LAST_UPDA]
7FE8CAE41E80 5F444554 45544144 454D4954 3A203D20  [TED_DATETIME = :]
""".trimIndent()

fun main(args: Array<String>) {
    val lines = input.split("\n")
    val sb = StringBuilder()
    lines.forEach { line ->
        val split = line.split(" ")
        val words = split.subList(1, 5)

        val wordsList = mutableListOf<String>()
        words.forEach {
            val reversedWord = it.substring(6, 8) + it.substring(4, 6) + it.substring(2, 4) + it.substring(0, 2)
            wordsList.add(reversedWord)

            // Convert each 2-digit hex number into it's ASCII equivalent
            // (in reverse order)
            sb.append((it.substring(6, 8).toInt(16).toChar()))
            sb.append((it.substring(4, 6).toInt(16).toChar()))
            sb.append((it.substring(2, 4).toInt(16).toChar()))
            sb.append((it.substring(0, 2).toInt(16).toChar()))
        }

        val outputLine = wordsList.joinToString(" ")
        println(outputLine)
    }

    println()
    println(sb.toString())
}
